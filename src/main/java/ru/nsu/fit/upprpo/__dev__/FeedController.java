package ru.nsu.fit.upprpo.__dev__;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.nsu.fit.upprpo.database.entities.Text;
import ru.nsu.fit.upprpo.database.repositories.TextRepository;

import javax.annotation.PostConstruct;

@Controller
public class FeedController {

    @Autowired private TextRepository textRepository;

    @PostConstruct
    private void feed() {
        if (textRepository.findAll().size() == 0) {
            textRepository.save(new Text("text 1"));
            textRepository.save(new Text("text 2"));
            textRepository.save(new Text("text 3"));
        }
    }
}
