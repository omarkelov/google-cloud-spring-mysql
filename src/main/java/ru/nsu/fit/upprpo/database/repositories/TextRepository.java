package ru.nsu.fit.upprpo.database.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.nsu.fit.upprpo.database.entities.Text;

import java.util.List;

public interface TextRepository extends CrudRepository<Text, Long> {

    List<Text> findAll();
}
