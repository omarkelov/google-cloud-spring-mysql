package ru.nsu.fit.upprpo.controllers;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.upprpo.database.repositories.TextRepository;

@RestController
@RequestMapping(value = "/api", produces = "application/json")
public class TextController {

    @Autowired private TextRepository textRepository;

    @GetMapping("/text")
    public String getText() {
        return new Gson().toJson(textRepository.findAll());
    }
}
