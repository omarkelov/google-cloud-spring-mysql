package ru.nsu.fit.upprpo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpprpoApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpprpoApplication.class, args);
	}
}
