import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './App.css';

import TextView from './text';

const App = () => {
    return (
        <BrowserRouter>
            <div className='App'>
                <Switch>
                    <Route exact path='/text' component={TextView} />
                </Switch>
            </div>
        </BrowserRouter>
    );
};

export default App;
