import { FC, useEffect, useState } from 'react';

export interface Text {
    id: number;
    text: string;
}

const TextView: FC = () => {
    const [texts, setTexts] = useState<Text[]>([]);

    const fetchTexts = () => {
        fetch('http://localhost:8080/api/text', { method: 'GET' })
            .then(response => response.json())
            .then((result: Text[]) => setTexts(result));
    };

    useEffect(() => {
        fetchTexts();
    }, []);

    return (
        <ul>
            {texts.map(text => <li>{`${text.id}: ${text.text}`}</li>)}
        </ul>
    );
};

export default TextView;
